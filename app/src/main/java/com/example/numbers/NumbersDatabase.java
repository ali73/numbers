package com.example.numbers;


import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.numbers.Model.Number;
import com.example.numbers.Model.NumberDao;

@Database(entities = {Number.class}, version = 1)
public abstract class NumbersDatabase extends RoomDatabase {

    private static NumbersDatabase db;


    public NumbersDatabase(){}
    public abstract NumberDao numberDao();

    public static NumbersDatabase getInstance(Context context){
        if (db == null){
            db = Room.databaseBuilder(context,
                    NumbersDatabase.class, "hinava-numbers").allowMainThreadQueries().build();
        }
        return db;
    }
}
