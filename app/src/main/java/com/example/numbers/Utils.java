package com.example.numbers;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import com.example.numbers.Model.AlarmReceiver;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

public class Utils {
    public static Date getDateFromStr(String str) throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd  HH:mm:ss");
        return format.parse(str);
    }

    public static String getDateinStr(Date date){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd  HH:mm:ss");
        return format.format(date);
    }

    public static Date getRandomTime(){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        int hour = new Random().nextInt(24);
        int minute = new Random().nextInt(60);
        int second = new Random().nextInt(60);
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE,1);
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE,minute);
        calendar.set(Calendar.SECOND,0);
        return calendar.getTime();
    }
    public static String getSharedPrefString(Context context, int id, String defualt){
        SharedPreferences sharedPref = context.getSharedPreferences(context.getResources().
                getString(R.string.SharedPrefName),Context.MODE_PRIVATE);
        return sharedPref.getString(context.getResources().getString(id),defualt);

    }
    public static void writeStringSharedPref(Context context, int id, String value){
        SharedPreferences sharedPref = context.getSharedPreferences(context.getResources().
                getString(R.string.SharedPrefName),Context.MODE_PRIVATE);
        sharedPref.edit().putString(context.getResources().getString(id),value).apply();
    }
    public static boolean isAlarmSet(Context context){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar calendar = Calendar.getInstance();
        String alarmDateStr =
                getSharedPrefString(context, R.string.AlarmDateTime,"1970-01-01 00:00:00");
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE,0);
        calendar.set(Calendar.SECOND,0);
        Calendar alarmDate = Calendar.getInstance();
        try{
            alarmDate.setTime(simpleDateFormat.parse(alarmDateStr));
            alarmDate.set(Calendar.HOUR_OF_DAY,0);
            alarmDate.set(Calendar.MINUTE,0);
            alarmDate.set(Calendar.SECOND,0);
            return calendar.compareTo(alarmDate) < 0;

        }
        catch (ParseException e){
            e.printStackTrace();
            return false;
        }
    }

    public static void setDailyAlarm(Activity context){
        if(!isAlarmSet(context)){
            Date date = getRandomTime();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            AlarmManager alarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
            PendingIntent alarmIntent;
            Intent intent = new Intent(context, AlarmReceiver.class);
            alarmIntent = PendingIntent.getBroadcast(context, 1, intent, PendingIntent.FLAG_UPDATE_CURRENT);
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (Build.VERSION.SDK_INT >= 23) {
                alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, date.getTime(), alarmIntent);
            }
            else {
                alarmManager.setExact(AlarmManager.RTC_WAKEUP, date.getTime(), alarmIntent);
            }
            Toast.makeText(context,"Alarm time:" + simpleDateFormat.format(date),Toast.LENGTH_LONG).show();
            writeStringSharedPref(context, R.string.AlarmDateTime,simpleDateFormat.format(date));
            Log.d("DailyAlarm",simpleDateFormat.format(date));
        }
    }
    public static long getRandomNumber(long minRange, long maxRange){
        float raw_rand = new Random().nextFloat();
        long rand = (long) (raw_rand * (maxRange-minRange));
        if(new Random().nextFloat() < 0.5f){
            rand += 1;
        }
        Log.d("Utils",String.valueOf(raw_rand));
        Log.d("Utils",String.valueOf(maxRange));
        Log.d("Utils",String.valueOf(minRange));
        Log.d("Utils",String.valueOf(rand));
        return rand;
    }
    public static boolean isNumberPrime(long number){
        long root = (long) Math.sqrt(number);
        for (int i = 2; i <= root; i++){
            if (number % i ==0)
                return false;
        }
        return true;
    }
}
