package com.example.numbers.Model;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import androidx.core.content.ContextCompat;

/**
 * To start number producing service {@link Timer} after device reboots.
 */
public class HinavaNumbersBootReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction()!=null &&
                intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED) ||
                intent.getAction().equals(Intent.ACTION_REBOOT)){
            Intent serviceIntent = new Intent(context, Timer.class);
            ContextCompat.startForegroundService(context, serviceIntent);
            Toast.makeText(context,"Boot completed",Toast.LENGTH_LONG).show();
        }

    }
}
