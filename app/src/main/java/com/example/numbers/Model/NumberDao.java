package com.example.numbers.Model;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface NumberDao {
    @Query("select * from number where isPrime = 1")
    LiveData<List<Number>> getPrimeNumbersLiveData();

    @Query("select * from number")
    LiveData<List<Number>> getAllNumbersLiveData();
    @Query("select * from number")
    List<Number> getAllNumbers();
    @Query("Select * from number order by creationDate DESC limit 1")
    LiveData<Number> getLastNumber();
    @Insert
    void insert(Number number);
    @Query("select count(*) from number where isPrime = 1")
    int getPrimeNumber();
    @Query("select count(*) from number where isPrime = 0")
    int getNonPrimeNumbers();
}
