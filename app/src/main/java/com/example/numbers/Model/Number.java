package com.example.numbers.Model;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.example.numbers.Utils.getDateFromStr;
import static com.example.numbers.Utils.getDateinStr;

/**
 * A class to for produced numbers.
 *
 * This class is a {@link Entity} class and will be stored in local database.
 */
@Entity
public class Number {
    private Long number;
    @PrimaryKey
    @NonNull
    private String creationDate;
    private Boolean isPrime;

    public Boolean isPrime() {
        return isPrime;
    }

    public void setPrime(Boolean prime) {
        isPrime = prime;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(@NonNull String creationDate) {
        this.creationDate = creationDate;
    }

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }
}
