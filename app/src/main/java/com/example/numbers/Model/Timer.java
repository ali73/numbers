package com.example.numbers.Model;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.database.sqlite.SQLiteConstraintException;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import com.example.numbers.Model.Number;
import com.example.numbers.NumbersDatabase;
import com.example.numbers.View.Activities.MainActivity;
import com.example.numbers.ViewModel.ViewModelNumbers;
import com.google.gson.Gson;

import java.util.Date;

import static com.example.numbers.Utils.getDateinStr;
import static com.example.numbers.Utils.getRandomNumber;
import static com.example.numbers.Utils.isNumberPrime;

/**
 * A foreground service to produce random numbers in background thread even if there is no visible activity of app.
 */
public class Timer extends Service {
    final String logTag = "numberTimer";
    Handler handler;
    Runnable runnable;
    public static final String CHANNEL_ID = "ForegroundServiceChannel";
    ViewModelNumbers viewModel;
    Number number;
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        handler = new Handler();
        NumbersDatabase db = NumbersDatabase.getInstance(getApplicationContext());
        viewModel = new ViewModelNumbers();
        runnable = new Runnable() {
            @Override
            public void run() {
                long randomNumber = getRandomNumber(0, 30000000000L);

                number = new Number();
                number.setNumber(randomNumber);
                number.setCreationDate(getDateinStr(new Date()));
                number.setPrime(isNumberPrime(randomNumber));
                try{
                    db.numberDao().insert(number);
                }
                catch (SQLiteConstraintException e){
                    Log.d(logTag,new Gson().toJson(number));
                    e.printStackTrace();
                }
                viewModel.getNumber().postValue(number.getNumber());
                viewModel.getCreationDate().postValue(number.getCreationDate());
                Log.d(logTag,"Timer working");
                Log.d(logTag,number.getCreationDate());
                handler.postDelayed(this, 30000);
            }
        };

        handler.postDelayed(runnable,0);

        createNotificationChannel();
        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                0, notificationIntent, 0);

        Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle("Foreground Service")
                .setContentText("my service running")
                .setContentIntent(pendingIntent)
                .build();

        startForeground(1, notification);
        return super.onStartCommand(intent, flags, startId);
    }
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel serviceChannel = new NotificationChannel(
                    CHANNEL_ID,
                    "Foreground Service Channel",
                    NotificationManager.IMPORTANCE_DEFAULT
            );

            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(serviceChannel);
        }
    }
}
