package com.example.numbers.View.Activities;

import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;

import com.example.numbers.Model.Number;
import com.example.numbers.NumbersDatabase;
import com.example.numbers.R;

import java.util.ArrayList;
import java.util.List;

import lecho.lib.hellocharts.model.PieChartData;
import lecho.lib.hellocharts.model.SliceValue;
import lecho.lib.hellocharts.view.PieChartView;

public class PieChartActivity extends AppCompatActivity {

    PieChartView chartView ;
    PieChartData chartData;
    List<SliceValue> values = new ArrayList<SliceValue>();
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistics);

        SliceValue primeValue = new SliceValue(0, Color.BLUE);
        SliceValue nonPrimeValue = new SliceValue(0,Color.RED);
        NumbersDatabase.getInstance(getApplicationContext()).numberDao().getLastNumber().observe(this,
                new Observer<Number>() {
                    @Override
                    public void onChanged(Number number) {
                        primeValue.setValue(NumbersDatabase.getInstance(getApplicationContext()).numberDao().getPrimeNumber())  ;
                        nonPrimeValue.setValue(NumbersDatabase.getInstance(getApplicationContext()).numberDao().getNonPrimeNumbers());
                        values.clear();
                        values.add(primeValue.setLabel("Prime-"+ primeValue.getValue()));
                        values.add(nonPrimeValue.setLabel("NonPrime-"+nonPrimeValue.getValue()));
                        chartData = new PieChartData(values);
                        chartData.setHasLabels(true);
                        chartData.setValueLabelTextSize(10);
                        chartData.setValueLabelsTextColor(Color.WHITE);
                        chartView.setPieChartData(chartData);
                    }
                });
//            values.add(primeValue);
            values.add(nonPrimeValue);

    }

    @Override
    protected void onResume() {
        super.onResume();
        chartView = findViewById(R.id.pie_chart_view);
        chartData = new PieChartData(values);
        chartData.setCenterText1("Hello");
        chartData.setCenterText2("world");
        chartView.setPieChartData(chartData);
        }
}
