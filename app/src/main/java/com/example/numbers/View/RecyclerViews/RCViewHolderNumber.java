package com.example.numbers.View.RecyclerViews;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.numbers.R;

public class RCViewHolderNumber extends RecyclerView.ViewHolder {
    private TextView number;
    private TextView date;
    public RCViewHolderNumber(@NonNull View itemView) {
        super(itemView);
        number = itemView.findViewById(R.id.rc_item_numberView_number);
        date = itemView.findViewById(R.id.rc_item_numberView_date);
    }

    public TextView getDate() {
        return date;
    }

    public TextView getNumber() {
        return number;
    }
}
