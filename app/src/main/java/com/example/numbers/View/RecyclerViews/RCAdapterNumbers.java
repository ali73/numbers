package com.example.numbers.View.RecyclerViews;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.numbers.Model.Number;
import com.example.numbers.R;

import java.util.ArrayList;

public class RCAdapterNumbers extends RecyclerView.Adapter<RCViewHolderNumber> {
    Context context;
    ArrayList<Number> _data;

    public RCAdapterNumbers(Context context){
        this.context = context;
        _data = new ArrayList<>();
    }
    @NonNull
    @Override
    public RCViewHolderNumber onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.rc_item_number, parent, false);
        return new RCViewHolderNumber(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RCViewHolderNumber holder, int position) {
        Number item = _data.get(position);
        holder.getDate().setText(item.getCreationDate());
        holder.getNumber().setText(String.valueOf(item.getNumber()));
    }

    @Override
    public int getItemCount() {
        return _data.size();
    }

    /**
     * Clean the data showing in recyclerview and add {@param list}.
     *
     * @param list List of numbers.
     */
    public void populate(ArrayList<Number> list){
        _data.clear();
        _data.addAll(list);
        notifyDataSetChanged();
    }

}
