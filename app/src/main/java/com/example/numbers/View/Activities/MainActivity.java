package com.example.numbers.View.Activities;

import android.content.Intent;
import android.os.Bundle;

import com.example.numbers.Model.Number;
import com.example.numbers.NumbersDatabase;
import com.example.numbers.R;
import com.example.numbers.Utils;
import com.example.numbers.View.RecyclerViews.RCAdapterNumbers;
import com.example.numbers.Model.Timer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import static com.example.numbers.Utils.setDailyAlarm;

public class    MainActivity extends AppCompatActivity {


    TextView lastNumberTV, nextSessionTV;
    Button prime_numbers;
    Button pie_chart;
    NumbersDatabase db;
    RecyclerView recyclerView;
    RCAdapterNumbers adapter;
    LiveData< List<Number>> liveData;
    boolean showingPrimes = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        db = NumbersDatabase.getInstance(getApplicationContext());

        Log.println(Log.DEBUG,"worker","msg");
        Intent intent = new Intent(getApplicationContext(), Timer.class);
        ContextCompat.startForegroundService(getApplicationContext(), intent);
        adapter = new RCAdapterNumbers(getApplicationContext());
        setDailyAlarm(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        lastNumberTV = findViewById(R.id.last_number);
        nextSessionTV = findViewById(R.id.alarm_time);
        prime_numbers = findViewById(R.id.prime);
        recyclerView = findViewById(R.id.numbers_list);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        liveData = db.numberDao().getAllNumbersLiveData();
        pie_chart = findViewById(R.id.pie_chart);
        db.numberDao().getLastNumber().observe(this, new Observer<Number>() {
            @Override
            public void onChanged(Number number) {
                if(number != null){
                    String text = String.format(getResources().getString(R.string.lastNumberText), String.valueOf(number.getNumber()), number.getCreationDate());
                    lastNumberTV.setText(text);
                }

            }
        });
        liveData.observe(this, new Observer<List<Number>>() {
            @Override
            public void onChanged(List<Number> numbers) {
                adapter.populate((ArrayList<Number>) numbers);
            }
        });
        prime_numbers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                liveData.removeObservers(MainActivity.this);
                if (!showingPrimes){
                    liveData = db.numberDao().getPrimeNumbersLiveData();
                    prime_numbers.setText(getResources().getString(R.string.all_numbers));
                }
                else {
                    liveData = db.numberDao().getAllNumbersLiveData();
                    prime_numbers.setText(getResources().getString(R.string.prime_numbers));
                }
                liveData.observe(MainActivity.this, new Observer<List<Number>>() {
                    @Override
                    public void onChanged(List<Number> numbers) {
                        adapter.populate((ArrayList)numbers);
                    }
                });
                showingPrimes = !showingPrimes;
            }
        });
        pie_chart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), PieChartActivity.class);
                startActivity(intent);
            }
        });

        String alarmDate = Utils.getSharedPrefString(getApplicationContext(),
                R.string.AlarmDateTime,null);
        if (alarmDate!=null){
            nextSessionTV.setText(String.format(getResources().getString(R.string.nextSession), alarmDate));
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
