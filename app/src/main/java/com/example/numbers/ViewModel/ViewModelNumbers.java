package com.example.numbers.ViewModel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.room.PrimaryKey;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ViewModelNumbers extends ViewModel {


    private MutableLiveData<Long> number;
    private MutableLiveData<String> creationDate;
    private MutableLiveData<Boolean> isPrime;



    public MutableLiveData<Long> getNumber() {
        if (number == null){
            number = new MutableLiveData<Long>();
        }
        return number;
    }

    public MutableLiveData<Boolean> getIsPrime() {
        if (isPrime == null){
            isPrime = new MutableLiveData<Boolean>();
        }
        return isPrime;
    }

    public MutableLiveData<String> getCreationDate() {
        if (creationDate == null){
            creationDate = new MutableLiveData<String>();
        }
        return creationDate;
    }



}
